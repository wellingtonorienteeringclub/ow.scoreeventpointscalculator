﻿using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Blazor.FileReader;
using BlazorFileSaver;
using IOF.XML.V3;
using Microsoft.AspNetCore.Components;

namespace OW.ScoreEventPointsCalculator.Pages {
  public class IndexBase : ComponentBase {
    protected string _Message = "";

    [Inject]
    protected IFileReaderService FileReaderService { get; set; }

    [Inject]
    protected IBlazorFileSaver BlazorFileSaver { get; set; }

    [Inject]
    protected IConfigurationService ConfigurationService { get; set; }

    [Inject]
    protected ICalculatorService CalculatorService { get; set; }

    [Inject]
    protected IResultFileGeneratorService ResultFileGeneratorService { get; set; }


    protected ElementReference InputTypeFileElement;

    protected Configuration _Configuration { get; set; }

    protected override async Task OnInitializedAsync() {
      _Configuration = await ConfigurationService.Load();
    }

    public async Task Process() {
      _Message = "";

      var resultList = await GetFile();

      if (resultList == null) {
        _Message = "Please upload a valid xml file!";
        return;
      }

      var result = CalculatorService.Calculate(_Configuration, resultList);
      var resultString = ResultFileGeneratorService.Generate(result);

      await BlazorFileSaver.SaveAs("result.html", resultString, "text/html");
    }

    public async Task Add() {
      _Configuration.ControlPointValues.Add(new ControlPointValue { ControlNumberEnd = 0, ControlNumberStart = 0, Points = 0 });
      await ConfigurationService.Save(_Configuration);
    }

    public async Task Remove(ControlPointValue controlPointValue) {
      _Configuration.ControlPointValues.Remove(controlPointValue);
      await ConfigurationService.Save(_Configuration);
    }

    public async Task Sort() {
      _Configuration.ControlPointValues = _Configuration.ControlPointValues.OrderBy(p => p.ControlNumberStart).ToList();
      await ConfigurationService.Save(_Configuration);
    }

    public async Task Save() {
      await ConfigurationService.Save(_Configuration);
    }

    private async Task<ResultList> GetFile() {
      foreach (var file in await FileReaderService.CreateReference(InputTypeFileElement).EnumerateFilesAsync()) {
        await using (Stream stream = await file.OpenReadAsync()) {
          var fi = await file.ReadFileInfoAsync();
          byte[] result = new byte[stream.Length];
          await stream.ReadAsync(result, 0, (int)stream.Length);

          if (fi.Type == "text/xml") {
            var resultFile = LoadFromXml(result);

            if (resultFile != null) {
              return resultFile;
            }
          }
        }
      }

      return null;
    }

    private ResultList LoadFromXml(byte[] rawFile) {
      Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
      string utf8String = Encoding.UTF8.GetString(Encoding.Convert(Encoding.GetEncoding(1252), Encoding.UTF8, rawFile));

      using (var stream = new StringReader(utf8String)) {
        using (var reader = XmlReader.Create(stream)) {
          return (ResultList)new XmlSerializer(typeof(ResultList)).Deserialize(reader);
        }
      }
    }
  }
}
