﻿using System.Collections.Generic;
using IOF.XML.V3;

namespace OW.ScoreEventPointsCalculator {
  public interface ICalculatorService {
    List<Entry> Calculate(Configuration configuration, ResultList resultList);
  }
}