﻿using System.Collections.Generic;
using System.Text;

namespace OW.ScoreEventPointsCalculator {
  public class ResultFileGeneratorService : IResultFileGeneratorService {

    public string Generate(List<Entry> results) {
      string currentGrade = "";

      StringBuilder sb = new StringBuilder();
      sb.Append("<html><head><title>Results</title></head><body><table><style>table{width:100%;} td{padding:5px 10px 5px 0;} th{text-align:left;}</style>");
      int position = 1;

      foreach (var e in results) {
        if (currentGrade != e.Grade) {
          sb.AppendLine($"</table>");
          sb.AppendLine($"<h2>{e.Grade}</h2>");
          sb.AppendLine($"<table><tr><th>Postition</th><th>Name</th><th>Club</th><th>Total Score</th><th>Time</th><th>Score</th><th>Penalty</th></tr>");

          position = 1;
          currentGrade = e.Grade;
        }

        sb.AppendLine($"<tr>");
        sb.AppendLine($"<td>{position++}</td>");
        sb.AppendLine($"<td>{e.Name}</td>");
        sb.AppendLine($"<td>{e.Club}</td>");
        sb.AppendLine($"<td>{e.TotalScore}</td>");
        sb.AppendLine($"<td>{e.Time}</td>");
        sb.AppendLine($"<td>{e.Score}</td>");
        sb.AppendLine($"<td>{e.ScoreAdjustment}</td>");
        sb.AppendLine($"</tr>");
      }

      sb.Append("</body></html>");

      return sb.ToString();
    }
  }
}
