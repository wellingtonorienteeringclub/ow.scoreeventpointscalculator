﻿using System.Collections.Generic;

namespace OW.ScoreEventPointsCalculator {
  public interface IResultFileGeneratorService {
    string Generate(List<Entry> results);
  }
}