﻿using System;
using System.Collections.Generic;
using System.Linq;
using IOF.XML.V3;

namespace OW.ScoreEventPointsCalculator {
  public class CalculatorService : ICalculatorService {
    public List<Entry> Calculate(Configuration configuration, ResultList resultList) {
      var maxTime = TimeSpan.FromMinutes(configuration.MaxTimeInMinutes);

      var entries = new List<Entry>();

      foreach (var g in resultList.ClassResult) {
        foreach (var p in g.PersonResult) {
          foreach (var r in p.Result) {
            int? score = null;
            int? totalScore = null;
            int scoreAdjustment = 0;

            var time = r.StartTimeSpecified && r.FinishTimeSpecified ? (TimeSpan?)(r.FinishTime - r.StartTime) : null;

            if (time.HasValue) {
              var controls = r.SplitTime
              .Where(s => s.Status == SplitTimeStatus.OK || s.Status == SplitTimeStatus.Additional)
              .Select(s => Convert.ToInt32(s.ControlCode))
              .Distinct();

              score = controls
              .Select(s => {
                var v = configuration.ControlPointValues.FirstOrDefault(c => c.ControlNumberStart <= s && c.ControlNumberEnd >= s);
                return v == null ? 0 : v.Points;
              }).Sum();

              if (time > maxTime) {
                var minutesPast = (time.Value - maxTime).Add(TimeSpan.FromSeconds(59)).Minutes;
                scoreAdjustment = minutesPast * configuration.PenaltyPerMinute;
                totalScore = score - scoreAdjustment;
              } else {
                totalScore = score;
              }
            }

            entries.Add(new Entry {
              Club = p?.Organisation?.Name,
              Name = (p?.Person?.Name?.Given + " " + p?.Person?.Name?.Family).Trim(),
              Grade = g?.Class?.Name,
              TotalScore = totalScore,
              ScoreAdjustment = scoreAdjustment,
              Score = score,
              Time = time
            });
          }
        }
      }

      return entries.Where(p => p.Score.HasValue && p.Time.HasValue).OrderBy(p => p.Grade).ThenByDescending(p => p.TotalScore.Value).ThenBy(p => p.Time.Value).ToList();
    }
  }
}
