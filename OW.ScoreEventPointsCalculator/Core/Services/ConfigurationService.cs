﻿using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;
using Blazored.LocalStorage;

namespace OW.ScoreEventPointsCalculator {
  public class ConfigurationService : IConfigurationService {
    private readonly ILocalStorageService _LocalStorageService;
    private const string KEY = "Configuration";

    public ConfigurationService(ILocalStorageService localStorageService) {
      _LocalStorageService = localStorageService;
    }

    public async Task<Configuration> Load() {
      Configuration configuration = await _LocalStorageService.GetItemAsync<Configuration>(KEY);

      if (configuration == null) {
        configuration = new Configuration {
          MaxTimeInMinutes = 60,
          PenaltyPerMinute = 10,
          ControlPointValues = new List<ControlPointValue> {
              new ControlPointValue { ControlNumberStart = 100, ControlNumberEnd = 109, Points = 1 },
              new ControlPointValue { ControlNumberStart = 110, ControlNumberEnd = 119, Points = 10 },
              new ControlPointValue { ControlNumberStart = 120, ControlNumberEnd = 129, Points = 20 },
              new ControlPointValue { ControlNumberStart = 130, ControlNumberEnd = 139, Points = 30 },
              new ControlPointValue { ControlNumberStart = 140, ControlNumberEnd = 149, Points = 40 },
              new ControlPointValue { ControlNumberStart = 150, ControlNumberEnd = 159, Points = 50 },
              new ControlPointValue { ControlNumberStart = 160, ControlNumberEnd = 169, Points = 60 },
              new ControlPointValue { ControlNumberStart = 170, ControlNumberEnd = 179, Points = 70 },
              new ControlPointValue { ControlNumberStart = 180, ControlNumberEnd = 189, Points = 80 },
              new ControlPointValue { ControlNumberStart = 190, ControlNumberEnd = 199, Points = 90 }
            }
        };

        await Save(configuration);
      }

      return configuration;
    }

    public async Task Save(Configuration configuration) {
      await _LocalStorageService.SetItemAsync(KEY, configuration);
    }
  }
}
