﻿using System.Threading.Tasks;

namespace OW.ScoreEventPointsCalculator {
  public interface IConfigurationService {
    Task<Configuration> Load();

    Task Save(Configuration configuration);
  }
}