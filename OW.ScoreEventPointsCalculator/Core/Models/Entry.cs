﻿using System;

namespace OW.ScoreEventPointsCalculator {
  public class Entry {
    public string Name { get; set; }
    public string Grade { get; set; }
    public string Club { get; set; }
    public TimeSpan? Time { get; set; }
    public int? Score { get; set; }
    public int? TotalScore { get; set; }
    public int ScoreAdjustment { get; set; }
  }
}
