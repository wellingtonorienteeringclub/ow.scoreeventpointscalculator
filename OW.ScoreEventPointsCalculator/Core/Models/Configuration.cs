﻿using System.Collections.Generic;

namespace OW.ScoreEventPointsCalculator {
  public class Configuration {
    public List<ControlPointValue> ControlPointValues { get; set; }
    public int MaxTimeInMinutes { get; set; }
    public int PenaltyPerMinute { get; set; }
  }
}
