using System.Threading.Tasks;
using Blazor.FileReader;
using Blazored.LocalStorage;
using BlazorFileSaver;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace OW.ScoreEventPointsCalculator {
  public class Program {
    public static async Task Main(string[] args) {
      var builder = WebAssemblyHostBuilder.CreateDefault(args);
      builder.RootComponents.Add<App>("app");

      builder.Services.AddBaseAddressHttpClient();
      builder.Services.AddFileReaderService(options => options.UseWasmSharedBuffer = true);
      builder.Services.AddBlazoredLocalStorage();
      builder.Services.AddScoped<IBlazorFileSaver, BlazorFileSaverJS>();
      builder.Services.AddBlazorFileSaver();
      builder.Services.AddScoped<IConfigurationService, ConfigurationService>();
      builder.Services.AddScoped<ICalculatorService, CalculatorService>();
      builder.Services.AddScoped<IResultFileGeneratorService, ResultFileGeneratorService>();

      await builder.Build().RunAsync();
    }
  }
}
